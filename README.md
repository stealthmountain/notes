# Notes

---
2019-06-19
Need data to illustrate styler app use. Points/lines/polygons.
* Elevation points

## Polygons from raster

1. Get [image](https://upload.wikimedia.org/wikipedia/commons/6/60/Morning%2C_Interior_-_Luce.jpeg)

2. Georeference, using desired srs (epsg 3857) [QGIS plugin FreehandRasterGeoreferencer](http://gvellut.github.io/FreehandRasterGeoreferencer/)

3. (Optional) Down-sample, nearest neighbor
```
gdalinfo Morning_Interior_Luce.jpeg 
Size is 3712, 2992
Coordinate System is:
PROJCS["WGS 84 / Pseudo-Mercator",...
Pixel Size = (72.705180655807595,-72.705180655807595)
```
 
`gdalwarp -co compress=lzw -tr 1163.28289049 1163.28289049 Morning_Interior_Luce.png MIL_16x.tif`

4. Reduce to 256 colors:

`rgb2pct.py px320_S_SLGJ_3857.png S_SLGJ_256.tif`

5. Convert raster cells to vector polygons (only does single band?)

`gdal_polygonize.py S_SLGJ_256.tif S_SLGJ_256.shp -b 1 -f "ESRI Shapefile" OUTPUT val`

5. (Optional) [Add values from other bands](https://gis.stackexchange.com/questions/292052/how-to-assign-raster-layers-colors-to-polygons-in-qgis
)

6. Add column for hex color

`ogrinfo -sql "ALTER TABLE S_SLGJ_256 ADD COLUMN hex text" S_SLGJ_256.shp`

7. Create csv from 256-color raster (hack):

`gdalinfo S_SLGJ_256.tif > pointilist.csv`

8. Edit csv... split on `:` and `,` to get separate columsn for `value, red, green, blue`; add another for `hex`

9. Calculate hex color from R, G, B values [SQL printf hex](https://stackoverflow.com/questions/15108932/c-the-x-format-specifier
)

`ogrinfo -dialect sqlite -sql "UPDATE pointilist SET hex = '#' || printf('%02X', r) || printf('%02X', g) || printf('%02X', b) " pointilist.csv`

10. Join to csv (CAST if necessary)

`ogrinfo S_SLGJ_256.shp -dialect sqlite -sql "update S_SLGJ_256 set hex = (select hex from 'pointilist.csv'.pointilist where CAST(val AS INT)=S_SLGJ_256.val)"`

11. Convert to geojson and reproject if needed

`ogr2ogr S_SLGJ_256.geojson S_SLGJ_256.shp -lco coordinate_precision=6 -s_srs 'PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"],AUTHORITY["EPSG","3857"]]' -t_srs 'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]'`

12. New styler feature: Data-Driven-Styling

---

